﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        //trojkat, prostokat
        static void Main(string[] args)
        {

        }
    }

    public class Trojkat : IFigura
    {
        public Trojkat(int Dlugosc_a, Punkt Zaczepienie)
        {
            this.Dlugosc_a = Dlugosc_a;
            this.Zaczepienie = Zaczepienie;
        }
        public Punkt Zaczepienie { get; set; }
        public int Dlugosc_a { get; set; }

        public List<Punkt> ZwrocPunkty()
        {
            var h = Dlugosc_a * Math.Sqrt(3) / 2;
            var result = new List<Punkt>();
            result.Add(new Punkt(Zaczepienie.X / 2, Zaczepienie.Y));
            result.Add(new Punkt(Zaczepienie.X, h));
            result.Add(new Punkt(Zaczepienie.X + Zaczepienie.X / 2, Zaczepienie.Y));
            return result;
        }

    }

    public class Prostokat : IFigura
    {
        public Prostokat(int Dlugosc_a, int Dlugosc_b, Punkt Zaczepienie)
        {
            this.Dlugosc_a = Dlugosc_a;
            this.Dlugosc_b = Dlugosc_b;
            this.Zaczepienie = Zaczepienie;
        }
        public List<Punkt> ZwrocPunkty()
        {
            var result = new List<Punkt>();
            result.Add(new Punkt(Zaczepienie.X, Zaczepienie.Y));
            result.Add(new Punkt(Zaczepienie.X, Zaczepienie.Y + Dlugosc_a));
            result.Add(new Punkt(Zaczepienie.X + Dlugosc_b, Zaczepienie.Y + Dlugosc_a));
            result.Add(new Punkt(Zaczepienie.X + Dlugosc_b, Zaczepienie.Y));
            return result;
        }
        public Punkt Zaczepienie { get; set; }
        public int Dlugosc_a { get; set; }
        public int Dlugosc_b { get; set; }
    }

    public class Punkt
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Punkt(double x, double y)
        {
            X = x;
            Y = y;
        }
    }

    public interface IFigura
    {
        List<Punkt> ZwrocPunkty();
        Punkt Zaczepienie { get; set; }
        int Dlugosc_a { get; set; }
    }
}