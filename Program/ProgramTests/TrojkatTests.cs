﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class TrojkatTests
    {
        [TestMethod()]
        public void PunktyTest()
        {
            var trojkat = new Trojkat(2, new Punkt(1, 1));
            Assert.AreEqual(1, trojkat.ZwrocPunkty()[1].X);
            Assert.AreEqual(Math.Sqrt(3), trojkat.ZwrocPunkty()[1].Y);
        }
        [TestMethod()]
        public void PunktyTestFails()
        {
            var trojkat = new Trojkat(2, new Punkt(1, 1));
            Assert.AreNotEqual(2, trojkat.ZwrocPunkty()[1].X);
            Assert.AreNotEqual(7, trojkat.ZwrocPunkty()[1].Y);
        }
    }
}